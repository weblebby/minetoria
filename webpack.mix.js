const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .js('resources/js/app.js', 'public/static/app/js')
    .js('resources/js/admin.js', 'public/static/admin/js')
    .sass('resources/sass/app.scss', 'public/static/app/css')
    .sass('resources/sass/admin.scss', 'public/static/admin/css')
    .version();
