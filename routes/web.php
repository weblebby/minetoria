<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

/**
 * Sitemap
 */
Route::as('sitemap.')->group(function () {
    Route::get('sitemap.xml', 'SitemapController@index')->name('index');
    Route::get('sitemap-categories.xml', 'SitemapController@categories')->name('categories');
    Route::get('sitemap-servers.xml', 'SitemapController@servers')->name('servers');
    Route::get('sitemap-server-posts.xml', 'SitemapController@serverPosts')->name('server.posts');
});

/**
 * Localize this routes.
 */
Route::group(['prefix' => LaravelLocalization::setLocale(), 'middleware' => 'localization'], function () {
    /**
     * Auth
     */
    Auth::routes(['verify' => true]);

    /**
     * Admin
     */
    Route::group([
        'prefix' => 'admin',
        'as' => 'admin.',
        'namespace' => 'Admin',
        'middleware' => ['auth', 'auth.admin']
    ], function () {
        /**
         * Overview
         */
        Route::get('', 'OverviewController@index')->name('overview');

        /**
         * Servers
         */
        Route::group(['prefix' => 'servers', 'as' => 'servers.'], function () {
            Route::get('', 'ServerController@index')->name('index');

            Route::group(['prefix' => '{server}'], function () {
                Route::put('toggle-suspended', 'ServerController@toggleSuspended')->name('toggle_suspended');
                Route::put('toggle-sponsored', 'ServerController@toggleSponsored')->name('toggle_sponsored');
            });
        });

        /**
         * Settings
         */
        Route::group(['prefix' => 'settings', 'as' => 'settings.'], function () {
            Route::get('', 'SettingsController@index')->name('index');
            Route::post('store', 'SettingsController@store')->name('store');
        });
    });

    /**
     * Servers
     */
    Route::as('servers.')->group(function () {
        /**
         * Server detail
         */
        Route::prefix('server/{server}')->as('show')->group(function () {
            Route::get('/', 'ServerController@show');
            Route::get('banner.png', 'ServerController@banner')->name('.banner');
        });

        /**
         * Create & Store
         */
        Route::middleware('auth')->get('create', 'ServerController@create')->name('create');
        Route::middleware('auth')->post('store', 'ServerController@store')->name('store');

        /**
         * Servers
         */
        Route::get('servers/{params?}', 'ServerController@index')->name('index');
        Route::get('{params?}', 'RedirectController@index'); // @TEMP
    });

    /**
     * Blog
     */
    Route::get('blog/servers/{blog}', 'BlogController@redirect');
    Route::get('servers/blog/{blog}', 'BlogController@show')->name('servers.blog.show');
});
