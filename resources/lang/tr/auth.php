<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Kimlik kontrol metinleri
    |--------------------------------------------------------------------------
    |
    | Aşağıdaki metinler kimlik doğrulama (giriş) sırasında kullanıcılara
    | gösterilebilecek mesajlardır. Bu metinleri uygulamanızın
    | gereksinimlerine göre düzenlemekte özgürsünüz.
    |
    */

    'failed' => 'Bu bilgilere ait bir hesap bulunamadı.',
    'throttle' => 'Çok fazla oturum açma girişiminde bulundunuz. Lütfen :seconds saniye sonra tekrar deneyiz.',
];
