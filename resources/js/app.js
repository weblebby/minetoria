window.Popper = require('popper.js');
window.$ = window.jQuery = require('jquery');
window.selectize = require('selectize');
window.ClipboardJS = require('clipboard');
require('bootstrap');

$(function () {
    $('[data-toggle=tooltip]').tooltip().on('click', function () {
        var title = $(this).data('original-title');
        var onClickTitle = $(this).data('onclick-title');

        $(this)
            .attr('data-original-title', onClickTitle)
            .tooltip('show')
            .attr('data-original-title', title);
    });
});

new ClipboardJS('.clipboard-js');