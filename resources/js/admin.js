window.Popper = require('popper.js');
window.$ = window.jQuery = require('jquery');
require('bootstrap');

$(function () {
    $('[data-toggle=tooltip]').tooltip();
});