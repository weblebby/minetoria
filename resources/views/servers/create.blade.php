@extends('servers.layouts.master')

@section('content')
<div class="container py-4">
    <div class="row">
        <div class="col-lg-6 offset-lg-3">
            <form action="{{ route('servers.store') }}" method="post">
                @csrf
                <div class="card mb-3">
                    <div class="card-body">
                        <h1 class="h5 mb-4">{{ __('Add server') }}</h1>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-9">
                                    <input type="text" name="server_ip"
                                        class="form-control @error('server_ip') is-invalid @enderror"
                                        value="{{ old('server_ip') }}" placeholder="{{ __('Server IP or hostname') }}"
                                        autofocus>
                                </div>
                                <div class="col-sm-3">
                                    <input type="text" name="server_port"
                                        class="form-control @error('server_port') is-invalid @enderror"
                                        placeholder="{{ __('Port') }}" value="{{ old('server_port', 25565) }}">
                                </div>
                            </div>
                            @error('server_ip')
                            <span class="invalid-feedback d-block">{{ $message }}</span>
                            @enderror
                            @error('server_port')
                            <span class="invalid-feedback d-block">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input type="text" name="name" class="form-control @error('name') is-invalid @enderror"
                                placeholder="{{ __('Server name') }}" value="{{ old('name') }}">
                            @error('name')
                            <span class="invalid-feedback d-block">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <select id="country" name="country" placeholder="{{ __('Choose country') }}">
                                <option></option>
                                @foreach ($countries as $country)
                                <option value="{{ $country->code }}"
                                    {{ old('country') === $country->code ? ' selected' : '' }}>
                                    {{ $country->with_native_name }}</option>
                                @endforeach
                            </select>
                            @error('country')
                            <span class="invalid-feedback d-block">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <select id="modes" name="modes[]" placeholder="{{ __('Choose game modes') }}" multiple>
                                @foreach($modes as $mode)
                                <option value="{{ $mode->id }}"
                                    {{ in_array($mode->id, old('modes', [])) ? ' selected' : '' }}>{{ $mode->name }}
                                </option>
                                @endforeach
                            </select>
                            @error('modes')
                            <span class="invalid-feedback d-block">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-footer">
                            <textarea name="description" rows="4"
                                class="form-control @error('description') is-invalid @enderror"
                                placeholder="{{ __('What things do you have on your server? Write in detail...') }}">{{ old('description') }}</textarea>
                            @error('description')
                            <span class="invalid-feedback d-block">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="card mb-3">
                    <div class="card-body">
                        <div class="form-group">
                            <h5 class="mb-4">{{ __('Optional fields') }}</h5>
                            <input type="text" name="website_url"
                                class="form-control @error('website_url') is-invalid @enderror"
                                placeholder="{{ __('Website') }}" value="{{ old('website_url') }}">
                            @error('website_url')
                            <span class="invalid-feedback d-block">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input type="text" name="discord_invite_url"
                                class="form-control @error('discord_invite_url') is-invalid @enderror"
                                placeholder="{{ __('Discord (invite link)') }}" value="{{ old('discord_invite_url') }}">
                            @error('discord_invite_url')
                            <span class="invalid-feedback d-block">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-footer">
                            <input type="text" name="video_url"
                                class="form-control @error('video_url') is-invalid @enderror"
                                placeholder="{{ __('YouTube video link') }}" value="{{ old('video_url') }}">
                            @error('video_url')
                            <span class="invalid-feedback d-block">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="card mb-3">
                    <div class="card-body">
                        <h5 class="mb-4">{{ __('Votifier (optional)') }}</h5>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="votifier_enabled"
                                name="votifier_enabled" {{ old('votifier_enabled') === 'on' ? ' checked' : '' }}>
                            <label class="custom-control-label"
                                for="votifier_enabled">{{ __('Enable votifier') }}</label>
                        </div>
                        <div id="votifier-area" class="mt-3" style="display:none">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-9">
                                        <input type="text" name="votifier_ip"
                                            class="form-control @error('votifier_ip') is-invalid @enderror"
                                            value="{{ old('votifier_ip') }}"
                                            placeholder="{{ __('Votifier IP or hostname') }}">
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" name="votifier_port"
                                            class="form-control @error('votifier_port') is-invalid @enderror"
                                            placeholder="{{ __('Port') }}" value="{{ old('votifier_port', 8192) }}">
                                    </div>
                                </div>
                                @error('votifier_ip')
                                <span class="invalid-feedback d-block">{{ $message }}</span>
                                @enderror
                                @error('votifier_port')
                                <span class="invalid-feedback d-block">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-footer">
                                <textarea name="votifier_public_key" rows="4"
                                    class="form-control @error('votifier_public_key') is-invalid @enderror"
                                    placeholder="{{ __('Votifier public key...') }}">{{ old('votifier_public_key') }}</textarea>
                                @error('votifier_public_key')
                                <span class="invalid-feedback d-block">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <button type="submit" class="btn btn-dark-2">{{ __('Continue') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    $(function () {
        $('#country').selectize();
        $('#modes').selectize();

        var votifier = function () {
            if ($('#votifier_enabled').prop('checked')) {
                $('#votifier-area').show();
            } else {
                $('#votifier-area').hide();
            }
        };

        $('#votifier_enabled').on('click', function () {
            votifier();
        });

        votifier();
    })
</script>
@endpush