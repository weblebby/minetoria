@extends('servers.layouts.master')

@section('content')
<section class="container py-4">
    <h1 class="section-title h4">{{ $page['title'] }}</h1>
</section>
<div class="container">
    <div class="row">
        <div class="col-lg-8">
            <section class="pb-4">
                <div class="row">
                    <div class="col-lg-12">
                        @if ($servers->count() > 0)
                            @foreach ($servers as $server)
                                @include('servers.components.server_card')
                            @endforeach
                            <div class="table-responsive">
                                {{ $servers->onEachSide(2)->withQueryString()->links() }}
                            </div>
                            <div class="mt-2 text-muted">
                                {{ __('Tracking :count servers', ['count' => number_format($trackingServersCount)]) }}
                            </div>
                        @else
                            <div class="card card-body text-muted">{{ __('Sorry. There is no server for this filters.') }}</div>
                        @endif
                    </div>
                </div>
            </section>
        </div>
        <div class="col-lg-4">
            <div class="card mb-3">
                <div class="card-body">
                    <h5 class="card-title">{{ __('Filter results') }}</h5>
                    <hr>
                    <div class="form-group">
                        <form method="GET">
                            <input type="text" name="q" class="form-control" placeholder="{{ __('Search...') }}"
                                value="{{ request('q') }}">
                        </form>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Country') }}</label>
                        <select id="filter-country" placeholder="{{ __('Choose a country') }}">
                            <option></option>
                            <option value="all">{{ __('All countries') }}</option>
                            @foreach ($countries as $country)
                            <option value="{{ $country->code }}"
                                {{ in_array($country->code, $params) ? ' selected' : '' }}>
                                {{ $country->with_native_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div>
                        <label>{{ __('Game') }}</label>
                        <div class="d-flex btn-filter-group">
                            @foreach ($modes as $mode)
                            <a href="{{ route('servers.index', implode(':', array_filter([$countryParam ? $countryParam->code : null, $modeParam && $modeParam->slug === $mode->slug ? null : $mode->slug]))) }}"
                                data-toggle="tooltip"
                                title="{{ $modeTitle = __(':mode (:count)', ['mode' => $mode->name, 'count' => $serversCount = number_format($mode->servers_count)]) }}"
                                class="d-flex justify-content-between align-items-center text-truncate btn btn-{{ in_array($mode->slug, $params) ? 'success' : 'dark-2' }}">
                                <span>{{ $mode->name }}</span>
                                <span class="server-count small">{{ $serversCount }}</span>
                            </a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="mb-3">
                @include('components.copyright')
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    $(function () {
        $('#filter-country').selectize();

        $('#filter-country').on('change', function () {
            var value = $(this).val();

            if (!value) {
                return;
            }

            if (value === 'all') {
                window.location = '{{ route('servers.index', $modeParam ? $modeParam->slug : null) }}';

                return;
            }

            window.location = '{{ route('servers.index', implode(':', array_filter([':country', $modeParam ? $modeParam->slug : null]))) }}'.replace(':country', value.toLowerCase());
        })
    })
</script>
@endpush