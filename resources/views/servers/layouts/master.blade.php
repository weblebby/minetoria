<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ mix('static/app/css/app.css') }}">
    @if ($favicon = setting('site.favicon'))
    <link rel="shortcut icon" href="{{ $favicon }}" />
    @endif
    {{ LSEO::generate() }}
    {!! setting('code.head') !!}
</head>

<body class="dark">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
            <a class="navbar-brand" href="{{ route('servers.index') }}">{{ setting('site.name') }}</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
                aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarText">
                <ul class="navbar-nav mr-auto">
                    @foreach($modes as $mode)
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('servers.index', $mode->slug) }}">{{ $mode->name }}</a>
                    </li>
                    @endforeach
                </ul>
                <ul class="navbar-nav ml-auto">
                    @guest
                    <li class="nav-item mr-2">
                        <a href="{{ route('login') }}"
                            class="btn btn-sm btn-outline-secondary box-round">{{ __('Sign in') }}</a>
                    </li>
                    @else
                    <li class="nav-item mr-2">
                        <div class="dropdown">
                            <a href="#" class="btn btn-sm btn-outline-secondary box-round"
                                data-toggle="dropdown">{{ auth()->user()->username }}</a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="#">{{ __('My servers') }}</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="javascript:;"
                                    onclick="return logoutForm.submit();">{{ __('Sign out') }}</a>
                            </div>
                        </div>
                    </li>
                    @endguest
                    <li class="nav-item">
                        <a href="{{ route('servers.create') }}"
                            class="btn btn-sm btn-success box-round">{{ __('Add your server') }}</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <main class="wrapper">
        @yield('content')
    </main>
    <footer class="footer">
        <div class="container">
            <div class="d-flex justify-content-between">
                <div>@lang('Made by <a href="https://www.fead.co" class="text-light" target="_blank">Fead.co</a>')</div>
                <div class="text-muted">hello@fead.co</div>
            </div>
        </div>
    </footer>
    <form action="{{ route('logout') }}" method="POST" id="logoutForm">@csrf</form>
    <script src="{{ mix('static/app/js/app.js') }}"></script>
    @stack('scripts')
    {!! setting('code.script') !!}
</body>

</html>