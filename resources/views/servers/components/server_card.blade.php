<div class="card mb-3">
    <div class="card-body">
        <div class="row">
            <div class="col-9 col-sm-10">
                <a href="{{ route('servers.show', $server->slug) }}" class="h5 text-light mb-0">{{ $server->name }}</a>
                <div class="server-meta-list">
                    <div class="server-meta-item">
                        <a href="javascript:;" class="text-link dashed clipboard-js"
                            data-clipboard-text="{{ $server->ipAndPort() }}" data-onclick-title="{{ __('Copied!') }}"
                            data-toggle="tooltip" title="{{ __('Click to copy') }}">{{ $server->ipAndPort() }}</a>
                    </div>
                    <div class="server-meta-item">
                        <span class="font-weight-bold">{{ number_format($server->online_players) }}</span>
                        <span class="text-muted">/</span>
                        <span class="text-muted">{{ number_format($server->max_players) }}</span>
                    </div>
                    @if ($server->firstVersion())
                    <div class="server-meta-item text-truncate" data-toggle="tooltip"
                        title="{{ $versionName = Str::stripMinecraftColor($server->firstVersion()->name) }}">
                        {{ $versionName }}
                    </div>
                    @endif
                </div>
            </div>
            <div class="col-3 col-sm-2 text-right">
                @if ($server->favicon)
                <img width="50" class="rounded-lg" src="{{ $server->faviconUrl() }}" alt="Server favicon">
                @endif
            </div>
        </div>
        <div class="d-flex flex-wrap">
            <span
                class="mr-1 mb-2 box-round btn-sm bg-{{ $server->isOnline() ? 'success' : 'danger' }}">{{ $server->isOnline() ? __('Online') : __('Offline') }}</span>
            @if ($server->country)
            <a href="{{ route('servers.index', $server->country) }}"
                class="mr-1 mb-2 box-round btn btn-sm btn-{{ $countryParam && $countryParam->code === $server->country ? 'success' : 'dark-2' }}">{{ $server->country()->name }}</a>
            @endif
            @foreach ($server->modes as $mode)
            <a href="{{ route('servers.index', $mode->slug) }}"
                class="mr-1 mb-2 box-round btn btn-sm btn-{{ $modeParam && $modeParam->slug === $mode->slug ? 'success' : 'dark-2' }}">{{ $mode->name }}</a>
            @endforeach
        </div>
    </div>
    <div style="margin-top:-0.5rem;"></div>
</div>