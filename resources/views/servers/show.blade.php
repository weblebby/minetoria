@extends('servers.layouts.master')

@section('content')
<div class="container py-4">
    <div class="row">
        <div class="col-lg-4">
            <div class="card mb-3 position-relative">
                @if ($server->favicon)
                <img width="60" src="{{ $server->faviconUrl() }}" class="server-favicon d-none d-sm-block"
                    alt="Server favicon">
                @endif
                <div class="card-body">
                    <div class="d-flex align-items-center justify-content-between">
                        <h1 class="h5 mb-0">{{ $server->name }}</h1>
                        @if ($server->favicon)
                        <img width="45" src="{{ $server->faviconUrl() }}" class="rounded-lg d-block d-sm-none"
                            alt="Server favicon">
                        @endif
                    </div>
                    <div class="row mt-4">
                        <div class="col-12">
                            <div class="btn-sm d-flex align-items-center justify-content-center box-round border border-primary w-100 mb-2"
                                data-toggle="tooltip"
                                title="{{ $onlinePlayers = number_format($server->online_players) }} / {{ $maxPlayers = number_format($server->max_players) }}">
                                <div class="text-truncate">
                                    <span class="font-weight-bold">{{ $onlinePlayers }}</span>
                                    <span class="text-muted mx-1">/</span>
                                    <span class="text-muted">{{ $maxPlayers }}</span>
                                </div>
                            </div>
                        </div>
                        @if ($server->firstVersion())
                        <div class="col-12">
                            <div class="btn-sm d-flex align-items-center justify-content-center box-round border border-primary w-100 mb-2"
                                data-toggle="tooltip"
                                title="{{ $versionName = Str::stripMinecraftColor($server->firstVersion()->name) }}">
                                <span class="text-truncate">{{ $versionName }}</span>
                            </div>
                        </div>
                        @endif
                        <div class="col-12">
                            <button
                                class="btn btn-primary btn-sm d-flex align-items-center justify-content-center box-round text-light w-100 clipboard-js"
                                data-clipboard-text="{{ $server->ipAndPort() }}"
                                data-onclick-title="{{ __('Copied!') }}" data-toggle="tooltip"
                                title="{{ __('Click to copy') }}">
                                <span>{{ $server->ipAndPort() }}</span>
                                <span class="ml-2" style="line-height: 1;">
                                    <svg class="bi bi-clipboard" width="1em" height="1em" viewBox="0 0 16 16"
                                        fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd"
                                            d="M4 1.5H3a2 2 0 00-2 2V14a2 2 0 002 2h10a2 2 0 002-2V3.5a2 2 0 00-2-2h-1v1h1a1 1 0 011 1V14a1 1 0 01-1 1H3a1 1 0 01-1-1V3.5a1 1 0 011-1h1v-1z"
                                            clip-rule="evenodd" />
                                        <path fill-rule="evenodd"
                                            d="M9.5 1h-3a.5.5 0 00-.5.5v1a.5.5 0 00.5.5h3a.5.5 0 00.5-.5v-1a.5.5 0 00-.5-.5zm-3-1A1.5 1.5 0 005 1.5v1A1.5 1.5 0 006.5 4h3A1.5 1.5 0 0011 2.5v-1A1.5 1.5 0 009.5 0h-3z"
                                            clip-rule="evenodd" />
                                    </svg>
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mb-3 px-3 py-1">
                <div class="table-responsive">
                    <table class="table mb-0">
                        <tbody>
                            <tr>
                                <td>
                                    <span class="text-warning small d-block lh-1 mb-1">{{ __('Status') }}</span>
                                    <div>
                                        <span class="text-light lh-1">
                                            <span
                                                class="server-status bg-{{ $server->isOnline() ? 'success' : 'danger' }}"></span>{{ $server->isOnline() ? __('Online') : __('Offline') }}
                                        </span>
                                    </div>
                                </td>
                            </tr>
                            @if ($server->website_url)
                            <tr>
                                <td>
                                    <span class="text-warning small d-block lh-1 mb-1">{{ __('Website') }}</span>
                                    <a href="{{ $server->website_url }}" class="text-light d-block lh-1"
                                        title="{{ $server->website_url }}" rel="noopener nofollow"
                                        target="_blank">{{ Str::limit($server->website_url, 30) }}</a>

                                </td>
                            </tr>
                            @endif
                            @if ($server->discord_url)
                            <tr>
                                <td>
                                    <span class="text-warning small d-block lh-1 mb-1">{{ __('Discord') }}</span>
                                    <a href="{{ $server->discord_url }}" class="text-light d-block lh-1"
                                        rel="noopener nofollow" target="_blank">{{ __('Click to join') }}</a>
                                </td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card mb-1">
                <div class="card-body px-3 py-2 mt-2">
                    <div class="d-flex flex-wrap">
                        @if ($server->country)
                        <a href="{{ route('servers.index', $server->country) }}"
                            class="btn btn-dark box-round bg-dark-2 mr-1 mb-2">{{ $server->country()->name }}</a>
                        @endif
                        @foreach ($server->modes as $mode)
                        <a href="{{ route('servers.index', $mode->slug) }}"
                            class="btn btn-dark box-round bg-dark-2 mr-1 mb-2">{{ $mode->name }}</a>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="mb-3">
                <span
                    class="text-muted small font-italic">{{ __('Last check: :date', ['date' => $server->pinged_at->diffForHumans()]) }}</span>
            </div>
        </div>
        <div class="col-lg-8">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="overview-tab" data-toggle="tab"
                        href="#overview">{{ __('Overview') }}</a>
                </li>
                @if ($server->video_url)
                <li class="nav-item">
                    <a class="nav-link" id="video-tab" data-toggle="tab" href="#trailer">{{ __('Trailer') }}</a>
                </li>
                @endif
                <li class="nav-item">
                    <a class="nav-link" id="banner-tab" data-toggle="tab" href="#banner">{{ __('Banner') }}</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane show active" id="overview">
                    <div class="card card-body card-nav-body">
                        @if ($server->motd)
                            <div class="server-motd">
                                <div class="server-motd--head">{{ __('Motd') }}</div>
                                <div class="server-motd--content">{{ $server->formatMotd() }}</div>
                            </div>
                        @endif
                        @if ($server->description)
                            <div class="server-description">{{ $server->formatDescription() }}</div>
                        @else
                            <span class="text-muted">{{ __('No description.') }}</span>
                        @endif
                    </div>
                </div>
                @if ($server->video_url)
                <div class="tab-pane" id="trailer">
                    <div class="card card-body card-nav-body">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item"
                                src="https://www.youtube.com/embed/{{ $server->video_url }}?rel=0"
                                allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
                @endif
                <div class="tab-pane" id="banner">
                    <div class="card card-body">
                        <div class="row">
                            <div class="col-lg-8">
                                <img width="100%" class="img-responsive rounded mb-3"
                                    src="{{ route('servers.show.banner', $server->slug) }}" alt="Server banner">
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <input type="text" class="form-control"
                                        value="{{ $bannerHtmlCode = '<a href="' . ($serverUrl = route('servers.show', $server->slug)) . '" title="' . ($bannerTitle = __(':server on :app', ['server' => $server->name, 'app' => setting('site.name')])) . '" target="_blank"><img src="' . ($serverBannerUrl = route('servers.show.banner', $server->slug)) . '" alt="' . $bannerTitle . '" width="540" height="90" /></a>' }}"
                                        readonly>
                                    <button class="btn btn-sm btn-dark-2 mt-2 clipboard-js"
                                        data-clipboard-text="{{ $bannerHtmlCode }}"
                                        data-onclick-title="{{ __('Copied!') }}"
                                        data-toggle="tooltip">{{ __('Copy HTML Code') }}</button>
                                </div>
                                <div class="form-footer">
                                    <input type="text" class="form-control"
                                        value="{{ $bannerBBCode = '[url=' . $serverUrl . '][img]' . $serverBannerUrl . '[/img][/url]' }}"
                                        readonly>
                                    <button class="btn btn-sm btn-dark-2 mt-2 clipboard-js"
                                        data-clipboard-text="{{ $bannerBBCode }}"
                                        data-onclick-title="{{ __('Copied!') }}"
                                        data-toggle="tooltip">{{ __('Copy BB Code') }}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection