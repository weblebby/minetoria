@extends('auth.layouts.master')

@section('content')
<div class="container">
    <div class="row pt-5">
        <div class="col-lg-6 offset-lg-3">
            <div class="text-center mb-5">
                <a href="{{ route('servers.index') }}" class="h2 text-light">{{ setting('site.name') }}</a>
            </div>
            <div class="card card-body">
                <div class="mb-4">
                    <h4 class="mb-1">{{ __('Sign in') }}</h4>
                    <span
                        class="text-muted">{{ __('Sign in to your :app account', ['app' => setting('site.name')]) }}</span>
                </div>
                <form action="{{ route('login') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <input type="email" id="email" name="email"
                            class="form-control form-control-lg @error('email') is-invalid @enderror"
                            value="{{ old('email') }}" placeholder="{{ __('e-Mail address') }}" autocomplete="email">
                        @error('email')
                        <span class="invalid-feedback">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group mb-1">
                        <input type="password" id="password" name="password"
                            class="form-control form-control-lg @error('password') is-invalid @enderror"
                            placeholder="{{ __('Password') }}" autocomplete="current-password">
                        @error('password')
                        <span class="invalid-feedback">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <a href="{{ route('password.request') }}" class="text-link">{{ __('Parolamı unuttum') }}</a>
                    </div>
                    <button type="submit" class="btn btn-dark-2">{{ __('Login') }}</button>
                </form>
            </div>
            <div class="text-center mt-3">
                {{ __("Don't an account?") }}
                <a href="{{ route('register') }}" class="text-warning">{{ __('Sign up now') }}</a>
            </div>
        </div>
    </div>
</div>
@endsection