@extends('auth.layouts.master')

@section('content')
<div class="container">
    <div class="row pt-5">
        <div class="col-lg-6 offset-lg-3">
            <div class="text-center mb-5">
                <a href="{{ route('servers.index') }}" class="h2 text-light">{{ setting('site.name') }}</a>
            </div>
            <div class="card card-body">
                <div class="mb-4">
                    <h4 class="mb-0">{{ __('Verify your email address') }}</h4>
                </div>
                @if (session('resent'))
                <div class="alert alert-success" role="alert">
                    {{ __('A fresh verification link has been sent to your email address.') }}
                </div>
                @endif
                <div class="mb-2">
                    {{ __('Before proceeding, please check your email for a verification link.') }}
                </div>
                <form method="POST" action="{{ route('verification.resend') }}">
                    @csrf
                    <button type="submit"
                        class="btn btn-sm btn-dark-2 text-link">{{ __('Resend verification link') }}</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection