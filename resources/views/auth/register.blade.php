@extends('auth.layouts.master')

@section('content')
<div class="container">
    <div class="row pt-5">
        <div class="col-lg-6 offset-lg-3">
            <div class="text-center mb-5">
                <a href="{{ route('servers.index') }}" class="h2 text-light">{{ setting('site.name') }}</a>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="mb-4">
                        <h4 class="mb-1">{{ __('Sign up') }}</h4>
                        <span
                            class="text-muted">{{ __('Create your :app account now', ['app' => setting('site.name')]) }}</span>
                    </div>
                    <form action="{{ route('register') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <input type="text" id="username" name="username"
                                class="form-control form-control-lg @error('username') is-invalid @enderror"
                                value="{{ old('username') }}" placeholder="{{ __('Minecraft username') }}"
                                autocomplete="nickname">
                            @error('username')
                            <span class="invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input type="email" id="email" name="email"
                                class="form-control form-control-lg @error('email') is-invalid @enderror"
                                value="{{ old('email') }}" placeholder="{{ __('e-Mail address') }}"
                                autocomplete="email">
                            @error('email')
                            <span class="invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input type="password" id="password" name="password"
                                class="form-control form-control-lg @error('password') is-invalid @enderror"
                                placeholder="{{ __('Password') }}" autocomplete="new-password">
                            @error('password')
                            <span class="invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-dark-2">{{ __('Register') }}</button>
                    </form>
                </div>
            </div>
            <div class="text-center mt-3">
                {{ __("Already have an account?") }}
                <a href="{{ route('login') }}" class="text-warning">{{ __('Sign in') }}</a>
            </div>
        </div>
    </div>
</div>
@endsection