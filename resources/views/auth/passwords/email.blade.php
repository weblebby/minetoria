@extends('auth.layouts.master')

@section('content')
<div class="container">
    <div class="row pt-5">
        <div class="col-lg-6 offset-lg-3">
            <div class="text-center mb-5">
                <a href="{{ route('servers.index') }}" class="h2 text-light">{{ setting('site.name') }}</a>
            </div>
            <div class="card card-body">
                <div class="mb-4">
                    <h4 class="mb-0">{{ __('Reset password') }}</h4>
                </div>
                @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
                @endif
                <form method="POST" action="{{ route('password.email') }}">
                    @csrf
                    <div class="form-group">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                            name="email" value="{{ old('email') }}" placeholder="{{ __('e-Mail address') }}" required
                            autocomplete="email" autofocus>
                        @error('email')
                        <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group mb-0">
                        <button type="submit" class="btn btn-dark-2">
                            {{ __('Reset password') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection