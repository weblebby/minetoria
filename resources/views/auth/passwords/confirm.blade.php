@extends('auth.layouts.master')

@section('content')
<div class="container">
    <div class="row pt-5">
        <div class="col-lg-6 offset-lg-3">
            <div class="text-center mb-5">
                <a href="{{ route('servers.index') }}" class="h2 text-light">{{ setting('site.name') }}</a>
            </div>
            <div class="card card-body">
                <div class="mb-4">
                    <h4 class="mb-1">{{ __('Confirm password') }}</h4>
                    <span class="text-muted">{{ __('Please confirm your password before continuing.') }}</span>
                </div>
                <form method="POST" action="{{ route('password.confirm') }}">
                    @csrf
                    <div class="form-group">
                        <input id="password" type="password"
                            class="form-control @error('password') is-invalid @enderror" name="password"
                            placeholder="{{ __('Password') }}" required autocomplete="current-password">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group mb-0">
                        <button type="submit" class="btn btn-dark-2">{{ __('Confirm password') }}</button>
                        @if (Route::has('password.request'))
                        <a class="btn btn-link text-link" href="{{ route('password.request') }}">
                            {{ __('Forgot your password?') }}
                        </a>
                        @endif
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection