<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('static/app/css/app.css') }}">
    @if ($favicon = setting('site.favicon'))
    <link rel="shortcut icon" href="{{ $favicon }}" />
    @endif
    {{ LSEO::generate() }}
    {!! setting('code.head') !!}
</head>

<body class="dark">
    <div class="wrapper">
        @yield('content')
    </div>
    {!! setting('code.script') !!}
</body>

</html>