<div class="small">
    <span class="text-muted">{{ date('Y') }} &mdash;</span>
    <span class="text-light">{{ __('All rights reserved.') }}</span>
</div>