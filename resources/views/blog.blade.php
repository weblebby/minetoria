@extends('servers.layouts.master')

@section('content')
<div class="container py-4">
    <h1 class="section-title h4">{{ $page['title'] }}</h1>
    <p class="w-50 text-muted">{{ $page['description'] }}</p>
    <div class="row">
        <div class="col-lg-8">
            <section class="pb-4">
                @foreach ($servers as $server)
                    <div class="card mb-3">
                        <div class="card-body">
                            <div class="d-flex align-items-center justify-content-between mb-3">
                                <h3 class="h5 mb-0">
                                    <a href="{{ route('servers.show', $server->slug) }}" class="text-light">{{ $loop->iteration }}. {{ $server->name }}</a>
                                </h3>
                                <div class="d-inline-block btn-sm box-round border border-primary">
                                    <div class="text-truncate">
                                        <span class="font-weight-bold">{{ number_format($server->online_players) }}</span>
                                        <span class="text-muted mx-1">/</span>
                                        <span class="text-muted">{{ number_format($server->max_players) }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="server-description small mb-3">{{ $server->formatDescription(200) }}</div>
                            <a href="{{ route('servers.show', $server->slug) }}">
                                <img class="img-responsive rounded" src="{{ route('servers.show.banner', $server->slug) }}" alt="Server banner">
                            </a>
                        </div>
                    </div>
                @endforeach
            </section>
        </div>
        <div class="col-lg-4">
            <div class="btn-filter-group mb-3">
                @foreach ($posts as $post)
                    <a href="{{ $post['url'] }}" class="w-100 d-flex align-items-center justify-content-between btn btn-{{ $post['mode_id'] === $mode->id ? 'success' : 'dark-2' }}">
                        <div class="text-truncate">{{ $post['title'] }}</div>
                        <span class="server-count">{{ $post['servers_count'] }}</span>
                    </a>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection