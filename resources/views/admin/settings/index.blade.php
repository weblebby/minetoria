@extends('admin.layouts.master')

@section('content')
<div class="wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="general-tab" data-toggle="tab"
                            href="#general">{{ __('General') }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="general-tab" data-toggle="tab"
                            href="#codes">{{ __('Styles & Scripts') }}</a>
                    </li>
                </ul>
                <form method="POST" action="{{ route('admin.settings.store') }}">
                    @csrf
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="general">
                            <div class="card card-body">
                                <div class="form-group row">
                                    <label for="site_name" class="col-md-4 col-form-label">{{ __('Site name') }}</label>
                                    <div class="col-md-8">
                                        <input id="site_name" name="site[name]" type="text"
                                            class="form-control @error('site.name') is-invalid @enderror"
                                            value="{{ old('site.name', setting('site.name')) }}" required autofocus>
                                        @error('site.name')
                                        <span class="invalid-feedback">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                @foreach ($locales as $localeCode => $locale)
                                <div class="form-group row">
                                    <label for="site_slogan_{{ $localeCode }}"
                                        class="col-md-4 col-form-label">{{ __('Slogan (:locale)', ['locale' => $locale['name']]) }}</label>
                                    <div class="col-md-8">
                                        <input id="site_slogan_{{ $localeCode }}" name="site[slogan][{{ $localeCode }}]"
                                            type="text" class="form-control @error(" site.slogan.{$localeCode}")
                                            is-invalid @enderror"
                                            value="{{ old("site.slogan.{$localeCode}", setting('site.slogan', $localeCode)) }}"
                                            required>
                                        @error("site.slogan.{$localeCode}")
                                        <span class="invalid-feedback">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                @endforeach
                                <hr>
                                <div class="form-group row mb-0">
                                    <label for="site_favicon"
                                        class="col-md-4 col-form-label">{{ __('Favicon (URL)') }}</label>
                                    <div class="col-md-8">
                                        <input id="site_favicon" name="site[favicon]" type="text"
                                            class="form-control @error('site.favicon') is-invalid @enderror"
                                            value="{{ old('site.favicon', setting('site.favicon')) }}">
                                        @error('site.favicon')
                                        <span class="invalid-feedback">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="codes">
                            <div class="card card-body">
                                <span class="text-muted">
                                    {{ __('This is the section where you can write extra CSS or add analytics codes.') }}
                                </span>
                                <hr>
                                <div class="form-group row">
                                    <label for="code_head"
                                        class="col-md-4 col-form-label">{{ __('Style codes') }}</label>
                                    <div class="col-md-8">
                                        <textarea id="code_head" name="code[head]"
                                            class="form-control @error('code.head') is-invalid @enderror"
                                            placeholder="{{ __('Will insert before the </head> tag') }}"
                                            rows="6">{{ old('code.head', setting('code.head')) }}</textarea>
                                        @error('code.head')
                                        <span class="invalid-feedback">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-0">
                                    <label for="code_script"
                                        class="col-md-4 col-form-label">{{ __('Script codes') }}</label>
                                    <div class="col-md-8">
                                        <textarea id="code_script" name="code[script]"
                                            class="form-control @error('code.script') is-invalid @enderror"
                                            placeholder="{{ __('Will insert before the </body> tag') }}"
                                            rows="6">{{ old('code.script', setting('code.script')) }}</textarea>
                                        @error('code.script')
                                        <span class="invalid-feedback">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mt-3">
                            <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection