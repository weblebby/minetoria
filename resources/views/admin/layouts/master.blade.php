<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <meta name="robots" content="noindex,nofollow,noarchive" />
    @if ($favicon = setting('site.favicon'))
    <link rel="shortcut icon" href="{{ $favicon }}" />
    @endif
    {{ LSEO::generate() }}
    <link rel="stylesheet" href="{{ mix('static/admin/css/admin.css') }}">
</head>

<body>
    <nav class="navbar navbar-expand-sm navbar-light">
        <div class="container">
            <a class="navbar-brand" href="{{ route('admin.overview') }}">{{ __('Dashboard') }}</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar"
                aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbar">
                <ul class="navbar-nav mr-auto mt-2 mt-sm-0">
                    <li class="nav-item{{ ($rotueName = request()->route()->getName()) === 'admin.overview' ? ' active' : '' }}">
                        <a class="nav-link" href="{{ route('admin.overview') }}">{{ __('Overview') }}</a>
                    </li>
                    <li class="nav-item{{ Str::contains($rotueName, 'admin.servers.') ? ' active' : '' }}">
                        <a class="nav-link" href="{{ route('admin.servers.index') }}">{{ __('Servers') }}</a>
                    </li>
                    <li class="nav-item{{ Str::contains($rotueName, 'admin.settings.') ? ' active' : '' }}">
                        <a class="nav-link" href="{{ route('admin.settings.index') }}">{{ __('Settings') }}</a>
                    </li>
                </ul>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="btn btn-sm btn-outline-secondary" href="{{ route('servers.index') }}"
                            target="_blank">{{ __('Go to site') }}</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="mb-5">
        @yield('content')
    </div>
    <script src="{{ mix('static/admin/js/admin.js') }}"></script>
    @stack('scripts')
</body>

</html>