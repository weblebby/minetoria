@extends('admin.layouts.master')

@section('content')
<div class="wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-4 form-group">
                <div class="card card-body">
                    <span class="text-muted text-uppercase font-weight-bold small">
                        <span class="circle bg-success mr-2"></span>{{ __('Online') }}
                    </span>
                    <span class="h1 mb-0">{{ $counters['online_servers'] }}</span>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 form-group">
                <div class="card card-body">
                    <span class="text-muted text-uppercase font-weight-bold small">
                        <span class="circle bg-danger mr-2"></span>{{ __('Offline') }}
                    </span>
                    <span class="h1 mb-0">{{ $counters['offline_servers'] }}</span>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 form-group">
                <div class="card card-body">
                    <span class="text-muted text-uppercase font-weight-bold small">
                        <span class="circle bg-warning mr-2"></span>{{ __('Sponsored') }}
                    </span>
                    <span class="h1 mb-0">{{ $counters['sponsored_servers'] }}</span>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 form-group">
                <div class="card card-body">
                    <span class="text-muted text-uppercase font-weight-bold small">
                        <span class="circle bg-secondary mr-2"></span>{{ __('Suspended') }}
                    </span>
                    <span class="h1 mb-0">{{ $counters['suspended_servers'] }}</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 form-group">
                <div class="card card-body">
                    <span class="text-muted text-uppercase font-weight-bold small">
                        {{ __('Total Users') }}
                    </span>
                    <span class="h1 mb-0">{{ $counters['total_users'] }}</span>
                </div>
            </div>
            <div class="col-md-6 form-group">
                <div class="card card-body">
                    <span class="text-muted text-uppercase font-weight-bold small">
                        {{ __('Online Players') }}
                    </span>
                    <span class="h1 mb-0">{{ $counters['online_players'] }}</span>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection