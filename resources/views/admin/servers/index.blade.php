@extends('admin.layouts.master')

@section('content')
<div class="wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="card">
                    <table class="table table-hover mb-0">
                        <thead>
                            <tr>
                                <th colspan="2">{{ __('Server') }}</th>
                                <th>{{ __('Players') }}</th>
                                <th>{{ __('Created at') }}</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($servers as $server)
                            <tr>
                                <td class="td-circle">
                                    <span class="circle bg-{{ $server->isOnline() ? 'success' : 'danger' }}"
                                        data-toggle="tooltip"
                                        title="{{ $server->isOnline() ? __('Online') : __('Offline') }}"></span>
                                </td>
                                <td>
                                    @if ($server->is_suspended)
                                    {{ $server->name }}
                                    @else
                                    <a class="text-dark" href="{{ route('servers.show', $server->slug) }}"
                                        target="_blank">{{ $server->name }}</a>
                                    @endif
                                    <div style="margin-top: -8px;">
                                        @if ($server->is_sponsored)
                                        <span class="small text-primary">{{ __('Sponsored') }}</span>
                                        @endif
                                        @if ($server->is_suspended)
                                        <span class="small text-muted">{{ __('Suspended') }}</span>
                                        @endif
                                    </div>
                                </td>
                                <td>
                                    {{ number_format($server->online_players) }}/{{ number_format($server->max_players) }}
                                </td>
                                <td>
                                    <span data-toggle="tooltip"
                                        title="{{ $server->created_at->format('Y-m-d H:i') }}">{{ $server->created_at->diffForHumans() }}</span>
                                </td>
                                <td>
                                    <div class="dropdown">
                                        <button type="button" class="btn btn-sm btn-outline-secondary"
                                            data-toggle="dropdown" style="line-height: .8;">
                                            <svg class="bi bi-three-dots" width="1em" height="1em" viewBox="0 0 16 16"
                                                fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd"
                                                    d="M3 9.5a1.5 1.5 0 110-3 1.5 1.5 0 010 3zm5 0a1.5 1.5 0 110-3 1.5 1.5 0 010 3zm5 0a1.5 1.5 0 110-3 1.5 1.5 0 010 3z"
                                                    clip-rule="evenodd" />
                                            </svg>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="#"
                                                onclick="return toggleServer('suspended', {{ $server->id }});">
                                                {{ $server->is_suspended ? __('Activate server') : __('Hide server') }}
                                            </a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="#"
                                                onclick="return toggleServer('sponsored', {{ $server->id }});">
                                                {{ $server->is_sponsored ? __('Remove sponsorship') : __('Make a sponsor') }}
                                            </a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="mt-3 mb-1">
                    <div class="table-responsive">
                        {{ $servers->onEachSide(2)->withQueryString()->links() }}
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card card-body">
                    <form method="GET" action="{{ route('admin.servers.index') }}">
                        <div class="form-group">
                            <input type="text" name="q" class="form-control" placeholder="{{ __('Search...') }}"
                                value="{{ request('q') }}">
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-6">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" name="sponsored" class="custom-control-input"
                                            id="filter-sponsored" {{ request('sponsored') === 'on' ? 'checked' : '' }}>
                                        <label class="custom-control-label"
                                            for="filter-sponsored">{{ __('Sponsored') }}</label>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" name="suspended" class="custom-control-input"
                                            id="filter-suspended" {{ request('suspended') === 'on' ? 'checked' : '' }}>
                                        <label class="custom-control-label"
                                            for="filter-suspended">{{ __('Suspended') }}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">{{ __('Search') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<form id="toggle-suspended-form" method="POST" action="{{ route('admin.servers.toggle_suspended', ':id') }}">
    @csrf
    @method('PUT')
</form>
<form id="toggle-sponsored-form" method="POST" action="{{ route('admin.servers.toggle_sponsored', ':id') }}">
    @csrf
    @method('PUT')
</form>
@endsection

@push('scripts')
<script>
    var toggleServer = function (action, id) {
        var $form = $('#toggle-' + action + '-form');

        $form.attr('action', $form.attr('action').replace(':id', id));
        
        $form.submit();

        return false;
    };
</script>
@endpush