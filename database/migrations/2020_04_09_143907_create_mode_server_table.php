<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModeServerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mode_server', function (Blueprint $table) {
            $table->id();
            $table->foreignId('mode_id');
            $table->foreignId('server_id');
            $table->timestamps();

            $table->foreign('mode_id')
                ->references('id')->on('modes')
                ->onDelete('cascade');

            $table->foreign('server_id')
                ->references('id')->on('servers')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mode_server');
    }
}
