<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('created_by')->nullable();
            $table->string('name');
            $table->string('slug');
            $table->string('server_ip');
            $table->string('server_original_ip');
            $table->string('server_port');
            $table->text('description')->nullable();
            $table->text('motd')->nullable();
            $table->string('country')->nullable();
            $table->integer('online_players');
            $table->integer('max_players');
            $table->string('favicon')->nullable();
            $table->string('website_url')->nullable();
            $table->string('discord_url')->nullable();
            $table->string('video_url')->nullable();
            $table->boolean('votifier_enabled');
            $table->string('votifier_ip')->nullable();
            $table->string('votifier_port')->nullable();
            $table->text('votifier_public_key')->nullable();
            $table->boolean('is_online');
            $table->boolean('is_sponsored')->default(false);
            $table->boolean('is_suspended')->default(false);
            $table->timestamp('pinged_at')->nullable();
            $table->timestamp('offline_at')->nullable();
            $table->timestamps();

            $table->foreign('created_by')
                ->references('id')->on('users')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servers');
    }
}
