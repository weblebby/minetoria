<?php

use App\Models\Mode;
use Illuminate\Database\Seeder;

class ModesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $modes = [
            'Anarchy',
            'Creative',
            'Economy',
            'Factions',
            'FTB',
            'Hardcore',
            'KitPvP',
            'MCMMO',
            'Mini Games',
            'Parkour',
            'Pixelmon',
            'Prison',
            'PvE',
            'PvP',
            'Raiding',
            'Roleplay',
            'Skyblock',
            'Skywars',
            'Survival',
            'Survival Games',
            'Tekkit',
            'Towny',
            'Vanilla',
        ];

        foreach ($modes as $mode) {
            Mode::create(['name' => $mode]);
        }
    }
}
