<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mode extends Model
{
    protected $fillable = [
        'name',
        'slug',
    ];

    public function servers()
    {
        return $this->belongsToMany(Server::class);
    }
}
