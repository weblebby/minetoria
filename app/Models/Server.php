<?php

namespace App\Models;

use App\Components\Country;
use App\Components\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\HtmlString;

class Server extends Model
{
    protected $fillable = [
        'name',
        'slug',
        'server_ip',
        'server_original_ip',
        'server_port',
        'description',
        'motd',
        'country',
        'online_players',
        'max_players',
        'favicon',
        'website_url',
        'discord_url',
        'video_url',
        'votifier_enabled',
        'votifier_ip',
        'votifier_port',
        'votifier_public_key',
        'is_sponsored',
        'is_suspended',
        'pinged_at',
        'offline_at',
    ];

    protected $casts = [
        'is_sponsored' => 'boolean',
        'is_suspended' => 'boolean',
        'server_port' => 'integer',
        'online_players' => 'integer',
        'max_players' => 'integer',
    ];

    protected $dates = [
        'pinged_at',
        'offline_at',
    ];

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function modes()
    {
        return $this->belongsToMany(Mode::class);
    }

    public function versions()
    {
        return $this->belongsToMany(Version::class);
    }

    public function scopeWhereOnline($query)
    {
        return $query->whereNull('offline_at');
    }

    public function scopeWhereOffline($query)
    {
        return $query->whereNotNull('offline_at');
    }

    public function scopeWhereTrackingServers($query)
    {
        return $query->whereOnline()->orWhereDate('offline_at', '>', today()->subDays(30));
    }

    public function scopeWhereSponsored($query)
    {
        return $query->where('is_sponsored', true);
    }

    public function scopeWhereNotSponsored($query)
    {
        return $query->where('is_sponsored', false);
    }

    public function scopeWhereSuspended($query)
    {
        return $query->where('is_suspended', true);
    }

    public function scopeWhereNotSuspended($query)
    {
        return $query->where('is_suspended', false);
    }

    public function country()
    {
        return Country::get()->where('code', $this->country)->first();
    }

    public function ipAndPort()
    {
        return $this->server_ip . ($this->server_port !== 25565 ? ":{$this->server_port}" : null);
    }

    public function isOnline()
    {
        return is_null($this->offline_at);
    }

    public function formatDescription(?int $limit = null)
    {
        if ($limit > 0) {
            $description =  Str::limit($this->description, $limit);
        }

        $description = preg_replace("/(?=\>\s+\n|\n)+(\s+)/", "\n", $description ?? $this->description);
        $description = array_map(fn ($paragraph) => '<p>' . e($paragraph, false) . '</p>', explode("\n", $description));

        return new HtmlString(implode("\n", $description));
    }

    public function formatMotd()
    {
        return new HtmlString(
            nl2br(Str::formatMinecraftColor(e($this->motd, false)))
        );
    }

    public function faviconPath()
    {
        return storage_path("app/public/favicons/{$this->id}{$this->favicon}.png");
    }

    public function faviconUrl()
    {
        return asset("storage/favicons/{$this->id}{$this->favicon}.png");
    }

    public function firstVersion()
    {
        return $this->versions->first();
    }
}
