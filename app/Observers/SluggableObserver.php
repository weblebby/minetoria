<?php

namespace App\Observers;

use App\Components\Str;

class SluggableObserver
{
    public function creating($model)
    {
        $model->slug = Str::uniqueSlug($model->name, $model, $model->slug);
    }
}
