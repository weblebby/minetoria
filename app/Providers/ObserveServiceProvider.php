<?php

namespace App\Providers;

use App\Observers\SluggableObserver;
use App\Models\Mode;
use App\Models\Server;
use Illuminate\Support\ServiceProvider;

class ObserveServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Mode::observe(SluggableObserver::class);
        Server::observe(SluggableObserver::class);
    }
}
