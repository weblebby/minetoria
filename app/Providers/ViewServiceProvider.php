<?php

namespace App\Providers;

use App\Models\Mode;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('servers.layouts.master', function ($view) {
            $modes = Mode::select('id', 'name', 'slug')
                ->has('servers')
                ->withCount('servers')
                ->latest('servers_count')
                ->limit(5)
                ->get();

            $view->with('modes', $modes);
        });
    }
}
