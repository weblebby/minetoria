<?php

use App\Components\Setting;

function setting($a = null, $b = null, $c = null)
{
    return app(Setting::class)->wizard($a, $b, $c);
}