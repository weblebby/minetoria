<?php

namespace App\Console\Commands;

use App\Jobs\CheckServer;
use App\Models\Server;
use Illuminate\Console\Command;

class CheckServers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'servers:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check informations for stored minecraft servers.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $servers = Server::select('id', 'server_ip', 'server_port')
            ->whereTrackingServers()
            ->whereNotSuspended()
            ->where('pinged_at', '<', now()->subMinutes(10))
            ->latest('pinged_at')
            ->get();

        $this->warn("Checks starting for {$servers->count()} servers.");

        foreach ($servers as $server) {
            CheckServer::dispatch($server);

            $this->info("{$server->server_ip} updated.");
        }

        $this->warn("Server checks done. {$servers->count()} servers is updated.");
    }
}
