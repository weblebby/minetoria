<?php

namespace App\Console\Commands;

use App\Components\Minecraft;
use App\Components\Str;
use App\Models\Mode;
use App\Models\Server;
use App\Models\Version;
use PHPHtmlParser\Dom;
use Illuminate\Console\Command;

class FetchServers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'servers:fetch {page?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch servers on remote website.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (app()->environment() !== 'local') {
            return $this->error('This command works only local environment.');
        }

        $this->fetchServers(
            $this->argument('page') ?: 1
        );
    }

    public function fetchServers($page = 1)
    {
        $baseUrl = 'https://minecraftservers.org';

        $dom = new Dom;
        $dom->loadFromUrl("{$baseUrl}/index/{$page}");

        $pagination = $dom->find('.pagination ul li a');
        $pagination = $pagination[count($pagination) - 1];
        $lastPage = str_replace("/index/", null, $pagination->href);

        if ($page > $lastPage) {
            return $this->info('Done! All servers fetched.');
        }

        $this->info("Page {$page} starting...");

        $serversContainer = $dom->find('table.serverlist');
        $serversContainerKey = count($serversContainer) === 1 ? 0 : 1;

        $servers = $serversContainer[$serversContainerKey]->find('tbody > tr');

        foreach ($servers as $server) {
            $serverNameDom = $server->find('h3.server-name > a');
            $serverIp = explode(':', $server->find('.server-ip > p')->text);

            $data = [
                'name' => $serverNameDom->text,
                'server_ip' => $serverIp[0],
                'server_port' => $serverIp[1] ?? 25565,
                'url' => $baseUrl . $serverNameDom->href,
                'is_online' => $server->find('.col-status .tag')->text === 'online',
            ];

            $serverOriginalIp = gethostbyname($data['server_ip']);

            $ipExistsInDatabase = Server::where(function ($query) use ($data, $serverOriginalIp) {
                $query->where('server_ip', $data['server_ip'])->orWhere('server_original_ip', $serverOriginalIp);
            })
                ->where('server_port', $data['server_port'])
                ->count() > 0;

            if ($ipExistsInDatabase) {
                $this->warn("{$data['name']} skipped because is exists.");

                continue;
            }

            $this->warn("{$data['name']} saving...");

            $minecraft = Minecraft::get($data['server_ip'], $data['server_port']);

            if ($minecraft === false || $data['is_online'] === false) {
                $this->info("{$data['name']} skipped because is offline.");

                continue;
            }

            $detailDom = new Dom;
            $detailDom->loadFromUrl($data['url']);

            $serverInfoDom = $detailDom->find('.server-info tr');
            $serverInfoDomCount = count($serverInfoDom);

            if ($serverInfoDom[3]->find('a')->count() > 0) {
                $data['website_url'] = $serverInfoDom[3]->find('a')->href;
            }

            if ($serverInfoDom[4]->find('a')->count() > 0) {
                $data['discord_url'] = $serverInfoDom[4]->find('a')->href;
            }

            $countryDom = $serverInfoDom[$serverInfoDomCount - 2]->find('.flag');

            if ($countryDom->count() > 0) {
                $data['country'] = Str::of($countryDom->class)->replace('flag ', null)->lower();
            }

            $modesDom = $serverInfoDom[$serverInfoDomCount - 1]->find('.tags a');

            foreach ($modesDom as $mode) {
                $data['modes'][] = $mode->text;
            }

            $trailerDom = $detailDom->find('#trailer #player');

            if ($trailerDom->count() > 0) {
                $data['video_url'] = $trailerDom->getAttribute('data-video-id');
            }

            $descriptionDom = $detailDom->find('p.desc');

            if ($descriptionDom->count() > 0) {
                $data['description'] = Str::of($descriptionDom->innerHtml)
                    ->trim()
                    ->replace('<br />', "\n");
            }

            $model = Server::create(array_merge($data, [
                'server_original_ip' => $serverOriginalIp,
                'votifier_enabled' => false,
                'motd' => $minecraft['description']['text'] ?? null,
                'online_players' => $minecraft['players']['online'],
                'max_players' => $minecraft['players']['max'],
                'favicon' => ($minecraft['favicon'] ?? false) ? Str::random(16) : null,
                'pinged_at' => now(),
            ]));

            if ($model->favicon) {
                $favicon = explode(',', $minecraft['favicon']);
                $favicon = base64_decode($favicon[1]);
                $favicon = imagecreatefromstring($favicon);

                imagepng($favicon, storage_path("app/public/favicons/{$model->id}{$model->favicon}.png"), 0);
            }

            if ($data['modes'] ?? false) {
                $modes = Mode::select('id')->whereIn('name', $data['modes'])->pluck('id');
                $model->modes()->attach($modes);
            }

            $version = Version::firstOrCreate(['name' => $minecraft['version']['name']]);
            $model->versions()->attach($version);

            $this->info("{$data['name']} saved.");
        }

        $this->fetchServers($page + 1);
    }
}
