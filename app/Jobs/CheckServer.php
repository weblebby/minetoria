<?php

namespace App\Jobs;

use App\Components\Minecraft;
use App\Components\Str;
use App\Models\Server;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CheckServer implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $server;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Server $server)
    {
        $this->server = $server;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $minecraft = Minecraft::get($this->server->server_ip, $this->server->server_port);

        $this->server->pinged_at = now();

        if ($minecraft === false) {
            if (is_null($this->server->offline_at)) {
                $this->server->offline_at = $this->server->pinged_at;
            }

            $this->server->online_players = 0;
            $this->server->max_players = 0;

            $this->server->save();

            return;
        }

        if (file_exists($this->server->faviconPath()) === false) {
            $this->server->favicon = ($minecraft['favicon'] ?? null) ? Str::random(16) : null;

            if ($this->server->favicon) {
                $favicon = explode(',', $minecraft['favicon']);
                $favicon = base64_decode($favicon[1]);
                $favicon = imagecreatefromstring($favicon);

                imagepng($favicon, $this->server->faviconPath(), 0);
            }
        }

        $this->server->motd = $minecraft['description']['text'] ?? null;
        $this->server->online_players = $minecraft['players']['online'];
        $this->server->max_players = $minecraft['players']['max'];
        $this->server->offline_at = null;

        $this->server->save();
    }
}
