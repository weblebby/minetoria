<?php

namespace App\Rules;

use App\Components\Country;
use Illuminate\Contracts\Validation\Rule;

class ExistsCountry implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $countries = Country::get()->pluck('code');

        return $countries->contains($value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('The selected country is invalid.');
    }
}
