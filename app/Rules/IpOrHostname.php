<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class IpOrHostname implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return filter_var(gethostbyname($value), FILTER_VALIDATE_IP) !== false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('Invalid IP or hostname.');
    }
}
