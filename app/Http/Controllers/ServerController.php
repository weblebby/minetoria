<?php

namespace App\Http\Controllers;

use Image;
use App\Components\LSEO;
use App\Components\Country;
use App\Components\Minecraft;
use App\Components\Str;
use App\Http\Requests\StoreServerRequest;
use App\Jobs\CheckServer;
use App\Models\Mode;
use App\Models\Server;
use App\Models\Version;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class ServerController
{
    public function index(Request $request, $params = null)
    {
        $trackingServersCount = Server::whereNotSuspended()
            ->whereTrackingServers()
            ->count();

        $servers = Server::whereNotSuspended()
            ->with(['modes', 'versions'])
            ->select([
                'id', 'name', 'slug', 'server_ip', 'server_port',
                'country', 'online_players', 'max_players',
                'favicon', 'offline_at',
            ])
            ->latest('online_players');

        $activeParamsCount = 0;
        $params = $params ? explode(':', $params) : [];

        $modeParam = Mode::select('id', 'name', 'slug')
            ->whereIn('slug', $params)
            ->first();

        if ($modeParam) {
            $servers->whereHas('modes', function ($query) use ($modeParam) {
                $query->where('modes.id', $modeParam->id);
            });

            $activeParamsCount++;
        }

        $allCountries = Country::get();

        $countryParam = $allCountries->filter(function ($country) use ($params) {
            return in_array($country->code, $params);
        })->first();

        if ($countryParam) {
            $servers->where('country', $countryParam->code);

            $activeParamsCount++;
        }

        if ($activeParamsCount !== count($params)) {
            return abort(404);
        }

        if ($request->has('q')) {
            $servers->where(function ($query) use ($request) {
                $query->where('server_ip', 'like', "%{$request->q}%")->orWhere('name', 'like', "%{$request->q}%");
            });
        }

        $serverCountries = ($modeParam ? $modeParam->servers() : new Server)
            ->select('country')
            ->whereNotSuspended()
            ->whereNotNull('country')
            ->pluck('country')
            ->unique()
            ->toArray();

        $countries = $allCountries->filter(function ($country) use ($serverCountries) {
            return in_array($country->code, $serverCountries);
        });

        $modes = Mode::select('name', 'slug')
            ->withCount(['servers' => function ($query) use ($countryParam) {
                if ($countryParam) {
                    $query->where('country', $countryParam->code);
                }
            }])
            ->having('servers_count', '>', 0)
            ->get();

        $servers = $servers->paginate();
        $totalServers = ['servers' => number_format($servers->total())];

        if ($countryParam && $modeParam) {
            $pageTransParams = ['mode' => $modeParam->name, 'country' => $countryParam->name];

            $page = [
                'title' => __('Minecraft :mode Servers in :country', $pageTransParams),
                'description' => __('Find Minecraft :mode Servers for :country. There are :servers active servers here.', $pageTransParams + $totalServers),
            ];
        } elseif ($countryParam) {
            $page = [
                'title' => __('Minecraft Servers in :country', ['country' => $countryParam->name]),
                'description' => __('Find Minecraft Servers for :country. There are :servers active servers here.', ['country' => $countryParam->name] + $totalServers),
            ];
        } elseif ($modeParam) {
            $page = [
                'title' => __('Minecraft :mode Servers', ['mode' => $modeParam->name]),
                'description' => __('Find Minecraft :mode Servers. There are :servers active servers here.', ['mode' => $modeParam->name] + $totalServers),
            ];
        } else {
            $page = [
                'title' => __('Minecraft Servers'),
                'description' => __('Find the best Minecraft servers with our multiplayer server list. Browse detailed information on each server and vote for your favourite.'),
                'set_title' => false,
            ];

            LSEO::setTitle('{sitename} {separator} {slogan}', false);
        }

        if ($page['set_title'] ?? true) {
            LSEO::setTitle($page['title']);
        }

        LSEO::setDescription($page['description']);

        LSEO::setCanonical(
            route(
                'servers.index',
                implode(':', array_filter([$countryParam ? $countryParam->code : null, $modeParam ? $modeParam->slug : null]))
            )
        );

        return view('servers.index', compact(
            'trackingServersCount',
            'servers',
            'modes',
            'countries',
            'params',
            'modeParam',
            'countryParam',
            'page'
        ));
    }

    public function show($slug)
    {
        $server = Server::select([
            'id', 'name', 'description', 'slug', 'server_ip', 'server_port',
            'country', 'online_players', 'max_players', 'pinged_at', 'website_url',
            'discord_url', 'video_url', 'favicon', 'motd', 'offline_at'
        ])
            ->whereNotSuspended()
            ->where('slug', $slug)
            ->firstOrFail();

        if ($server->pinged_at < now()->subMinutes(10)) {
            CheckServer::dispatch($server);

            return redirect()->refresh();
        }

        LSEO::setTitle($server->name);

        LSEO::setDescription(__(":name. :modes. Up to :max players. :status now.", [
            'name' => $server->name,
            'modes' => $server->modes->pluck('name')->join(', '),
            'max' => number_format($server->max_players),
            'status' => $server->isOnline() ? __('Available') : __('Unavailable'),
        ]));

        LSEO::setCanonical(
            route('servers.show', $slug)
        );

        return view('servers.show', compact(
            'server'
        ));
    }

    public function banner($slug)
    {
        return Cache::remember("banners.{$slug}", now()->addHour(), function () use ($slug) {
            $server = Server
                ::select([
                    'id', 'name', 'server_ip', 'server_port',
                    'online_players', 'max_players',
                    'country', 'favicon', 'offline_at'
                ])
                ->with(['modes' => function ($query) {
                    $query->select('modes.id', 'name');
                }])
                ->where('slug', $slug)
                ->firstOrFail();

            $onlinePlayers = number_format($server->online_players);
            $maxPlayers = number_format($server->max_players);

            $image = Image::make(resource_path('images/mc-banner-1-540x90.png'));

            $image->blur(3);

            if ($server->favicon) {
                $faviconPosition = 13;
                $faviconWidth = 64;

                $image->insert($server->faviconPath(), 'left-center', $faviconPosition);
            }

            $startPosition = $server->favicon ? ($faviconPosition + $faviconWidth + 10) : 13;

            $texts = [
                [
                    'x' => $startPosition, 'y' => 13,
                    'value' => Str::limit($server->name, 17),
                    'size' => 30,
                    'valign' => 'top',
                    'color' => '#fff',
                    'shadow' => true,
                    'weight' => 'bold',
                ],
                [
                    'x' => $startPosition, 'y' => 44,
                    'value' => $server->ipAndPort(),
                    'size' => 18,
                    'color' => '#fff',
                    'shadow' => true,
                    'valign' => 'top',
                ],
                [
                    'x' => $startPosition, 'y' => 76,
                    'value' => Str::limit($server->modes->pluck('name')->join(' | '), 63),
                    'size' => 14,
                    'color' => '#fff',
                    'shadow' => true,
                ],
                [
                    'x' => 527, 'y' => 13,
                    'value' => $server->isOnline() ? __('Online') : __('Offline'),
                    'size' => 15,
                    'color' => $server->isOnline() ? '#28a745' : '#dc3545',
                    'shadow' => [0, 0, 0, .3],
                    'align' => 'right',
                    'valign' => 'top',
                    'weight' => 'bold',
                ],
                [
                    'x' => 527, 'y' => 32,
                    'value' => "{$onlinePlayers}/{$maxPlayers}",
                    'size' => 14,
                    'color' => '#fff',
                    'shadow' => true,
                    'align' => 'right',
                    'valign' => 'top',
                ]
            ];

            if ($server->country) {
                $texts[] = [
                    'x' => 527, 'y' => 76,
                    'value' => Str::upper($server->country()->code),
                    'size' => 14,
                    'color' => '#fff',
                    'shadow' => true,
                    'align' => 'right',
                ];
            }

            foreach ($texts as $text) {
                $text['weight'] = $text['weight'] ?? 'regular';

                if ($text['shadow'] ?? false) {
                    $image->text($text['value'], $text['x'], $text['y'] + 2, function ($font) use ($text) {
                        $font->file(resource_path("fonts/google/google-{$text['weight']}.ttf"));
                        $font->size($text['size']);

                        if ($text['valign'] ?? false) {
                            $font->valign($text['valign']);
                        }

                        if ($text['align'] ?? false) {
                            $font->align($text['align']);
                        }

                        $font->color(is_array($text['shadow']) ? $text['shadow'] : [0, 0, 0, .6]);
                    });

                    $image->text($text['value'], $text['x'], $text['y'], function ($font) use ($text) {
                        $font->file(resource_path("fonts/google/google-{$text['weight']}.ttf"));
                        $font->size($text['size']);
                        $font->color($text['color']);

                        if ($text['valign'] ?? false) {
                            $font->valign($text['valign']);
                        }

                        if ($text['align'] ?? false) {
                            $font->align($text['align']);
                        }
                    });
                }
            }

            return $image->response('png');
        });
    }

    public function create()
    {
        $modes = Mode::get();
        $countries = Country::get();

        LSEO::setTitle(__('Add new server'));
        LSEO::setDescription(__('Do you have a server? Add your server and promote it to thousands of users.'));
        LSEO::setCanonical(route('servers.create'));

        return view('servers.create', compact('countries', 'modes'));
    }

    public function store(StoreServerRequest $request)
    {
        $validated = collect($request->validated());

        $minecraft = Minecraft::get($validated['server_ip'], $validated['server_port']);

        if ($minecraft === false) {
            return back()->withErrors(['server_ip' => __('Connection is not valid.')])->withInput();
        }

        $serverOriginalIp = gethostbyname($validated['server_ip']);

        $ipExistsInDatabase = Server::where(function ($query) use ($serverOriginalIp, $validated) {
            $query->where('server_ip', $validated['server_ip'])->orWhere('server_original_ip', $serverOriginalIp);
        })
            ->where('server_port', $validated['server_port'])
            ->count() > 0;

        if ($ipExistsInDatabase) {
            return back()->withErrors(['server_ip' => __('This server is already stored.')])->withInput();
        }

        $data = $validated->only([
            'name',
            'server_ip',
            'server_port',
            'description',
            'website_url',
            'discord_url',
            'video_url',
        ]);

        $data = $data->merge([
            'server_original_ip' => $serverOriginalIp,
            'country' => $validated['country'],
            'motd' => $minecraft['description']['text'] ?? null,
            'online_players' => $minecraft['players']['online'],
            'max_players' => $minecraft['players']['max'],
            'votifier_enabled' => $validated['votifier_enabled'] ?? false,
            'pinged_at' => now(),
        ]);

        if ($data['votifier_enabled'] === true) {
            $data = $data->merge($validated->only([
                'votifier_ip',
                'votifier_port',
                'votifier_public_key',
            ]));
        }

        $server = new Server($data->toArray());

        if ($minecraft['favicon'] ?? false) {
            $favicon = explode(',', $minecraft['favicon']);
            $favicon = base64_decode($favicon[1]);
            $favicon = imagecreatefromstring($favicon);

            $server->favicon = Str::random(16);

            imagepng($favicon, storage_path("app/public/favicons/{$server->id}{$server->favicon}.png"), 0);
        }

        $server->createdBy()->associate(auth()->user());

        $server->save();

        $version = Version::firstOrCreate(['name' => $minecraft['version']['name']]);
        $server->versions()->attach($version);

        foreach ($validated['modes'] as $mode) {
            $server->modes()->attach($mode);
        }

        return redirect()->route('servers.show', $server->slug);
    }
}
