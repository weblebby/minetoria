<?php

namespace App\Http\Controllers;

use App\Components\Country;
use App\Models\Mode;
use App\Models\Server;
use App\Services\ServerBlogService;

class BlogController extends Controller
{
    public function redirect(string $slug)
    {
        return redirect()->route('servers.blog.show', $slug, 301);
    }

    public function show(string $slug, ServerBlogService $service)
    {
        $parseSlug = $service->parseSlug($slug);

        if (is_null($parseSlug)) {
            abort(404);
        }

        $mode = Mode::where('slug', $parseSlug['mode'])->firstOrFail();

        if (!is_null($parseSlug['country'])) {
            $country = Country::get()->firstWhere('slug', $parseSlug['country']);

            if (is_null($country)) {
                abort(404);
            }
        }

        $servers = Server::query()
            ->whereHas('modes', fn ($query) => $query->where('modes.id', $mode->id))
            ->where('is_suspended', false)
            ->when($country ?? false, fn ($query) => $query->where('country', $country->code))
            ->latest('online_players')
            ->whereOnline()
            ->take(35)
            ->get();

        if ($servers->count() < 5) {
            abort(404);
        }

        $posts = $service->recommendPost($mode, $country ?? NULL);

        $page = $service->makeSeo($slug, $mode, $country ?? NULL, $servers->count());

        return view('blog', compact('page', 'mode', 'servers', 'posts'));
    }
}
