<?php

namespace App\Http\Controllers\Admin;

use App\Components\LSEO;
use App\Http\Controllers\Controller;
use App\Models\Server;
use App\Models\User;

class OverviewController extends Controller
{
    public function index()
    {
        $onlineServers = Server::whereOnline()->whereNotSuspended()->count();
        $offlineServers = Server::whereOffline()->whereNotSuspended()->count();
        $sponsoredServers = Server::whereSponsored()->whereNotSuspended()->count();
        $suspendedServers = Server::whereSuspended()->count();
        $onlinePlayers = Server::whereOnline()->whereNotSuspended()->sum('online_players');
        $totalUsers = User::count();

        $counters = [
            'online_servers' => number_format($onlineServers),
            'offline_servers' => number_format($offlineServers),
            'sponsored_servers' => number_format($sponsoredServers),
            'suspended_servers' => number_format($suspendedServers),
            'online_players' => number_format($onlinePlayers),
            'total_users' => number_format($totalUsers),
        ];

        LSEO::setTitle(__('Overview'));

        return view('admin.overview', compact(
            'counters'
        ));
    }
}
