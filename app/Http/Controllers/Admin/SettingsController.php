<?php

namespace App\Http\Controllers\Admin;

use App\Components\LSEO;
use App\Components\Setting;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreSettingRequest;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class SettingsController extends Controller
{
    public function index()
    {
        $settings = Setting::getAll();
        $locales = LaravelLocalization::getSupportedLocales();

        LSEO::setTitle(__('Settings'));

        return view('admin.settings.index', compact(
            'settings',
            'locales'
        ));
    }

    public function store(StoreSettingRequest $request)
    {
        $data = $request->validated();

        setting([
            'site.name' => $data['site']['name'],
            'site.favicon' => $data['site']['favicon'],
            'code.head' => $data['code']['head'],
            'code.script' => $data['code']['script'],
        ]);

        foreach ($data['site']['slogan'] as $locale => $slogan) {
            setting(['site.slogan' => ['value' => $slogan, 'locale' => $locale]]);
        }

        return back();
    }
}
