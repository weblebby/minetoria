<?php

namespace App\Http\Controllers\Admin;

use App\Components\LSEO;
use App\Models\Server;
use Illuminate\Http\Request;

class ServerController
{
    public function index(Request $request)
    {
        $servers = Server::select([
            'id', 'name', 'slug', 'server_ip', 'server_port',
            'online_players', 'max_players', 'is_sponsored', 'is_suspended',
            'created_at', 'offline_at'
        ])
            ->latest('created_at');

        if ($request->has('q')) {
            $servers->where('name', 'like', "%{$request->q}%");
        }

        if ($request->has('sponsored')) {
            $servers->where('is_sponsored', (bool) $request->sponsored);
        }

        if ($request->has('suspended')) {
            $servers->where('is_suspended', (bool) $request->suspended);
        }

        $servers = $servers->paginate(25);

        LSEO::setTitle(__('Servers'));

        return view('admin.servers.index', compact(
            'servers'
        ));
    }

    public function toggleSuspended($id)
    {
        $server = Server::select('id', 'is_suspended')->findOrFail($id);

        $server->is_suspended = !$server->is_suspended;

        $server->save();

        return back();
    }

    public function toggleSponsored($id)
    {
        $server = Server::select('id', 'is_sponsored')->findOrFail($id);

        $server->is_sponsored = !$server->is_sponsored;

        $server->save();

        return back();
    }
}
