<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RedirectController extends Controller
{
    public function index($params = null)
    {
        return redirect()->route('servers.index', $params, 301);
    }
}
