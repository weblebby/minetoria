<?php

namespace App\Http\Controllers;

use App\Components\Country;
use LaravelLocalization;
use App\Components\Str;
use App\Models\Mode;
use App\Models\Server;

class SitemapController extends Controller
{
    protected $sluggedAppName;

    public function __construct()
    {
        $this->sluggedAppName = Str::slug(setting('site.name', null, 'minetoria'), '_');
        $this->locales = collect(LaravelLocalization::getSupportedLocales());
    }

    public function index()
    {
        $sitemap = app('sitemap');

        $sitemap->setCache("{$this->sluggedAppName}.sitemap.index", 60 * 60);

        if (!$sitemap->isCached()) {
            $sitemap->addSitemap(route('sitemap.categories'), '2020-04-11');
            $sitemap->addSitemap(route('sitemap.servers'), '2020-04-11');
            $sitemap->addSitemap(route('sitemap.server.posts'), '2020-04-11');
        }

        return $sitemap->render('sitemapindex');
    }

    public function categories()
    {
        $sitemap = app('sitemap');

        $sitemap->setCache("{$this->sluggedAppName}.sitemap.categories", 60 * 60);

        if (!$sitemap->isCached()) {
            $modes = Mode::with(['servers' => function ($query) {
                $query
                    ->select('servers.id', 'country')
                    ->whereNotSuspended()
                    ->whereNotNull('country');
            }])
                ->select('id', 'slug', 'updated_at')
                ->get();

            foreach ($this->locales as $localeCode => $locale) {
                foreach ($modes as $mode) {
                    $sitemap->add(
                        $url = LaravelLocalization::localizeUrl(route('servers.index', $mode->slug), $localeCode),
                        $mode->updated_at,
                        '1.0',
                        'monthly',
                        [],
                        null,
                        $this->locales->map(function ($locale, $key) use ($url) {
                            return ['language' => $key, 'url' => LaravelLocalization::localizeUrl($url, $key)];
                        })->toArray()
                    );

                    $countries = $mode->servers
                        ->pluck('country')
                        ->unique();

                    foreach ($countries as $country) {
                        $sitemap->add(
                            $url = LaravelLocalization::localizeUrl(route('servers.index', "{$country}:{$mode->slug}"), $localeCode),
                            $mode->updated_at,
                            '1.0',
                            'monthly',
                            [],
                            null,
                            $this->locales->map(function ($locale, $key) use ($url) {
                                return ['language' => $key, 'url' => LaravelLocalization::localizeUrl($url, $key)];
                            })->toArray()
                        );
                    }
                }
            }
        }

        return $sitemap->render('xml');
    }

    public function servers()
    {
        $sitemap = app('sitemap');

        $sitemap->setCache("{$this->sluggedAppName}.sitemap.servers", 60 * 60);

        if (!$sitemap->isCached()) {
            $servers = Server::select('slug', 'updated_at')
                ->whereNotSuspended()
                ->get();

            foreach ($this->locales as $localeCode => $locale) {
                foreach ($servers as $server) {
                    $sitemap->add(
                        $url = LaravelLocalization::localizeUrl(route('servers.show', $server->slug), $localeCode),
                        $server->updated_at,
                        '0.9',
                        'monthly',
                        [],
                        null,
                        $this->locales->map(function ($locale, $key) use ($url) {
                            return ['language' => $key, 'url' => LaravelLocalization::localizeUrl($url, $key)];
                        })->toArray()
                    );
                }
            }
        }

        return $sitemap->render('xml');
    }

    public function serverPosts()
    {
        $sitemap = app('sitemap');

        $sitemap->setCache("{$this->sluggedAppName}.sitemap.server.posts", 60 * 60 * 24 * 60);

        if (!$sitemap->isCached()) {
            $modes = Mode::query()
                ->withCount(['servers' => fn ($query) => $query->whereOnline()])
                ->having('servers_count', '>', 4)
                ->get();

            $countries = Country::get()->whereIn(
                'code',
                Server::select('country')->whereNotNull('country')->groupBy('country')->pluck('country')
            );

            foreach ($this->locales as $localeCode => $locale) {
                foreach ($modes as $mode) {
                    $sitemap->add(
                        $url = LaravelLocalization::localizeUrl(route('servers.blog.show', "minecraft-{$mode->slug}-servers"), $localeCode),
                        $mode->updated_at,
                        '0.9',
                        'monthly',
                        [],
                        null,
                        $this->locales->map(function ($locale, $key) use ($url) {
                            return ['language' => $key, 'url' => LaravelLocalization::localizeUrl($url, $key)];
                        })->toArray()
                    );
                }

                foreach ($countries as $country) {
                    $modes = Mode::query()
                        ->withCount(['servers' => function ($query) use ($country) {
                            if ($country) {
                                $query->where('country', $country->code)->whereOnline();
                            }
                        }])
                        ->having('servers_count', '>', 4)
                        ->get();

                    foreach ($modes as $mode) {
                        $sitemap->add(
                            $url = LaravelLocalization::localizeUrl(route('servers.blog.show', "minecraft-{$mode->slug}-servers-{$country->slug}"), $localeCode),
                            $mode->updated_at,
                            '0.9',
                            'monthly',
                            [],
                            null,
                            $this->locales->map(function ($locale, $key) use ($url) {
                                return ['language' => $key, 'url' => LaravelLocalization::localizeUrl($url, $key)];
                            })->toArray()
                        );
                    }
                }
            }
        }

        return $sitemap->render('xml');
    }
}
