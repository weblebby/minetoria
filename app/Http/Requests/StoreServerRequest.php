<?php

namespace App\Http\Requests;

use App\Rules\ExistsCountry;
use App\Rules\IpOrHostname;
use Illuminate\Foundation\Http\FormRequest;

class StoreServerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'server_ip' => ['required', 'string', new IpOrHostname],
            'server_port' => ['required', 'numeric'],
            'name' => ['required', 'string', 'max:191'],
            'country' => ['required', new ExistsCountry],
            'modes' => ['required', 'array'],
            'modes.*' => ['exists:modes,id'],
            'description' => ['required', 'string', 'min:50', 'max:1000'],
            'website_url' => ['nullable', 'active_url'],
            'discord_url' => ['nullable', 'active_url'],
            'video_url' => ['nullable', 'active_url'],
            'votifier_enabled' => ['nullable', 'boolean'],
            'votifier_ip' => ['nullable', 'required_if:votifier_enabled,on', 'string', new IpOrHostname],
            'votifier_port' => ['nullable', 'required_if:votifier_enabled,on', 'numeric'],
            'votifier_public_key' => ['nullable', 'required_if:votifier_enabled,on', 'string', 'max:500'],
        ];
    }
}
