<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class StoreSettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() && auth()->user()->is_admin === true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'site.name' => ['required', 'string', 'max:191'],
            'site.slogan' => ['required', 'array'],
            'site.slogan.*' => ['string', 'max:191'],
            'site.favicon' => ['nullable', 'url', 'max:191'],
            'code.head' => ['nullable', 'string', 'max:2000'],
            'code.script' => ['nullable', 'string', 'max:2000'],
        ];
    }
}
