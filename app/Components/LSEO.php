<?php

namespace App\Components;

use LaravelLocalization;
use Illuminate\Support\HtmlString;
use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\SEOTools;

class LSEO
{
    protected static $initialize;

    protected static function initialize()
    {
        if (self::$initialize === null) {
            return self::$initialize = new self();
        }

        return self::$initialize;
    }

    public static function generate()
    {
        return new HtmlString(SEOTools::generate(true));
    }

    public static function setTitle($title, $default = true)
    {
        $initialize = self::initialize();

        $variables = $initialize->variables();

        if ($default === false) {
            SEOTools::setTitle(
                str_replace(
                    array_keys($variables),
                    array_values($variables),
                    $title
                )
            );

            return;
        }

        SEOTools::setTitle(
            implode(' ', [
                $title,
                $variables['{separator}'],
                $variables['{sitename}']
            ])
        );
    }

    public static function setCanonical($url)
    {
        $locales = LaravelLocalization::getSupportedLocales();

        foreach ($locales as $key => $locale) {
            if ($key === LaravelLocalization::getCurrentLocale()) {
                continue;
            }

            SEOMeta::addAlternateLanguage($key, LaravelLocalization::localizeUrl($url, $key));
        }

        SEOMeta::addAlternateLanguage('x-default', LaravelLocalization::localizeUrl($url, 'en'));

        return SEOTools::setCanonical($url);
    }

    public static function __callStatic($name, $args)
    {
        return SEOTools::$name(...$args);
    }

    protected function variables()
    {
        $locale = app()->getLocale();

        return [
            '{sitename}' => setting('site.name'),
            '{slogan}' => setting('site.slogan', $locale),
            '{separator}' => config('seotools.meta.defaults.separator')
        ];
    }
}
