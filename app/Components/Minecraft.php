<?php

namespace App\Components;

use xPaw\MinecraftPing;
use xPaw\MinecraftPingException;

class Minecraft
{
    public static function get($ip, $port = 25565)
    {
        try {
            $minecraft = new MinecraftPing($ip, $port);
            
            $query = $minecraft->query();
            
            $minecraft->close();

            if (isset($query['players']) === false) {
                return false;
            }

            return $query;
        } catch (MinecraftPingException $e) {
            return false;
        }
    }
}