<?php

namespace App\Components;

use Illuminate\Support\Str as Base;

class Str extends Base
{
    public static function uniqueSlug($title, $model, $slug = null, $column = 'slug')
    {
        if (is_null($slug)) {
            $slug = self::slug($title);
        }

        $base = $slug;

        if (is_array($model) === false) {
            $model = [$model];
        }

        foreach ($model as $value) {
            $i = 1;

            while ($value::where($column, $slug)->first() !== null) {
                $slug = $base . '-' . $i++;
            }
        }

        return $slug;
    }

    public static function formatMinecraftColor($string)
    {
        $string = utf8_decode(htmlspecialchars($string, ENT_QUOTES, "UTF-8"));
        $string = preg_replace('/\xA7([0-9a-f])/i', '<span class="mc-color mc-$1">', $string, -1, $count) . str_repeat("</span>", $count);

        return utf8_encode(preg_replace('/\xA7([k-or])/i', '<span class="mc-$1">', $string, -1, $count) . str_repeat("</span>", $count));
    }

    public static function stripMinecraftColor($string)
    {
        $string = utf8_decode(htmlspecialchars($string, ENT_QUOTES, "UTF-8"));
        $string = preg_replace('/\xA7([0-9a-f])/i', null, $string, -1, $count);

        return utf8_encode(preg_replace('/\xA7([k-or])/i', null, $string, -1, $count));
    }
}
