<?php

namespace App\Components;

class Country
{
    public static function get()
    {
        if ($countries = config('countries')) {
            return $countries;
        }

        $path = resource_path('countries.json');
        $countries = collect(json_decode(file_get_contents($path)));

        config(['countries' => $countries]);

        return $countries->map(function ($country) {
            $nativeName = isset($country->native_name) ? " ({$country->native_name})" : null;

            $country->with_native_name = $country->name . $nativeName;

            return $country;
        });
    }
}