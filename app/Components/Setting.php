<?php

namespace App\Components;

use App\Models\Setting as SettingModel;

class Setting
{
    protected static $settings;

    public static function wizard($a = null, $b = null, $c = null)
    {
        // This method works for setting() method in "app/helpers.php"

        // Redirect to "getAll" method
        if (is_null($a)) {
            return self::getAll();
        }

        // Redirect to "set" method
        if (is_array($a)) {
            $settings = [];

            foreach ($a as $key => $setting) {
                $value = is_array($setting) ? $setting['value'] : $setting;

                $settings[] = self::set($key, $value, $setting['locale'] ?? null);
            }

            return $settings;
        }

        // Redirect to "get" method
        if (is_string($a)) {
            return self::get($a, $b, $c);
        }
    }

    public static function getAll()
    {
        if (is_null(self::$settings)) {
            return self::$settings = SettingModel::select('key', 'value', 'locale')->get()->mapWithKeys(function ($setting) {
                $key = $setting->key;

                if ($setting->locale) {
                    $key .= ".{$setting->locale}";
                }

                return [$key => $setting->value];
            });
        }

        return self::$settings;
    }

    public static function get($key, $locale = null, $default = null)
    {
        $setting = self::getAll()->filter(function ($setting, $i) use ($key, $locale) {
            return $i === $key . ($locale ? ".{$locale}" : null);
        })
            ->first();

        if (is_null($setting)) {
            return $default;
        }

        return $setting;
    }

    public static function set($key, $value, $locale = null)
    {
        if (is_null($value)) {
            return self::forget($key, $locale);
        }

        return SettingModel::updateOrCreate(['key' => $key, 'locale' => $locale], ['value' => $value]);
    }

    public static function forget($key, $locale = null)
    {
        $setting = SettingModel::where('key', $key)
            ->where('locale', $locale)
            ->first();

        if (is_null($setting)) {
            return false;
        }

        $setting->delete();

        return true;
    }
}
