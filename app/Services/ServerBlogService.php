<?php

namespace App\Services;

use App\Components\LSEO;
use App\Components\Str;
use App\Models\Mode;

class ServerBlogService
{
    public function makeSeo(string $slug, Mode $mode, ?object $country, int $serversCount): array
    {
        if (is_null($country)) {
            $page = [
                'title' => __('Top :count Minecraft :mode Servers', ['mode' => $mode->name, 'count' => $serversCount]),
                'description' => __('We have listed the best Minecraft :mode servers with the most players. Browse servers or add your own!', ['mode' => $mode->name])
            ];
        } else {
            $page = [
                'title' => __('Top :count Minecraft :mode Servers in :country', ['mode' => $mode->name, 'count' => $serversCount, 'country' => $country->name]),
                'description' => __('We have listed the best Minecraft :mode servers in :country with the most players. Browse servers or add your own!', ['mode' => $mode->name, 'country' => $country->name])
            ];
        }

        LSEO::setTitle($page['title']);
        LSEO::setDescription($page['description']);
        LSEO::setCanonical(route('servers.blog.show', $slug));

        return $page;
    }

    public function parseSlug(string $slug): ?array
    {
        preg_match('/' . __('minecraft-:mode-servers:country', ['mode' => '([a-z-]+)', 'country' => '(?:([a-z-]+)?)']) . '/', $slug, $matches);

        if (count($matches) < 2) {
            return NULL;
        }

        return [
            'mode' => $matches[1],
            'country' => Str::replaceFirst('-', '', $matches[2] ?? NULL),
        ];
    }

    public function recommendPost(Mode $mode, ?object $country): array
    {
        $modes = Mode::query()
            ->withCount(['servers' => function ($query) use ($country) {
                if ($country) {
                    $query->where('country', $country->code);
                }

                $query->whereOnline();
            }])
            ->having('servers_count', '>', 4)
            ->take(15)
            ->get();

        $posts = [];

        foreach ($modes as $mode) {
            $posts[] = [
                'title' => __('Minecraft :mode Servers' . ($country ? ' in :country' : ''), [
                    'mode' => $mode->name,
                    'country' => $country->name ?? NULL,
                ]),
                'url' => route(
                    'servers.blog.show',
                    __('minecraft-:mode-servers:country', [
                        'mode' => $mode->slug,
                        'country' => $country ? "-{$country->slug}" : ''
                    ])
                ),
                'servers_count' => $mode->servers_count > 35 ? 35 : $mode->servers_count,
                'mode_id' => $mode->id,
            ];
        }

        return $posts;
    }
}
